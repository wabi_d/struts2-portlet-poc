package cz.zcu.kiv.jet.strutsportlet.actions;

import java.util.Arrays;
import java.util.List;

import javax.portlet.PortletPreferences;

import org.apache.struts2.dispatcher.DefaultActionSupport;
import org.apache.struts2.portlet.interceptor.PortletPreferencesAware;

import com.opensymphony.xwork2.Preparable;

import cz.zcu.kiv.jet.strutsportlet.data.DataStorage;
import cz.zcu.kiv.jet.strutsportlet.data.Gender;
import cz.zcu.kiv.jet.strutsportlet.data.Person;

public class HelloAction extends DefaultPortletAction  {

	private static final long serialVersionUID = 1L;

	/**
	 * Portlet preference expo
	 */
	private String firstName;
	/**
	 * Portlet preference expo
	 */
	private String lastName;
	
	private List<Gender> genders = Arrays.asList(Gender.values());
	
	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}
	
	public List<Person> getPeople() {
		return storage.getPeople();
	}
	
	public List<Gender> getGenders() {
		return genders;
	}
	
	public void setGenders(List<Gender> genders) {
		this.genders = genders;
	}


	@Override
	public String execute() throws Exception {
		firstName = getPortletPreferences().getValue("firstName", "");
		lastName = getPortletPreferences().getValue("lastName", "");
		return SUCCESS;
	}
}

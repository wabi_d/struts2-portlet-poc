package cz.zcu.kiv.jet.strutsportlet.actions;

import javax.portlet.PortletPreferences;

import org.apache.struts2.dispatcher.DefaultActionSupport;
import org.apache.struts2.portlet.interceptor.PortletPreferencesAware;

import cz.zcu.kiv.jet.strutsportlet.data.DataStorage;

public class DefaultPortletAction extends DefaultActionSupport implements PortletPreferencesAware {
	
	private PortletPreferences portletPreferences;
	
	/**
	 * Dummy datastorage.
	 */
	protected DataStorage storage = DataStorage.getDataStorage();
	
	public void setPortletPreferences(PortletPreferences prefs) {
		this.portletPreferences = prefs;
	}
	
	public PortletPreferences getPortletPreferences() {
		return portletPreferences;
	}

}

package cz.zcu.kiv.jet.strutsportlet.actions;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.Action;

import cz.zcu.kiv.jet.strutsportlet.data.DataStorage;
import cz.zcu.kiv.jet.strutsportlet.data.Person;

/**
 * This class handles whole jQuery Grid tag. Provides data and arguments.
 * 
 * IMPORTANT: MUSTN'T extend struts Action class
 * 
 * @author J. Danek
 */
public class LoadTableData {
	
	private DataStorage dataStorage = DataStorage.getDataStorage();
	
	// data to display, send from a place somewhere else
	// e.g. far far away - acutally they have to be loaded
	//from dataStorage (or session, or maybe some other possibility howto
	//pass the value)
	private List<Integer> selectedPeople;

	// table data model - what should the table display on the current page
	// MUST have this name
	private List<Person> gridModel;

	// get how many rows we want to have into the grid - rowNum attribute in the
	// grid
	private Integer rows = 0;

	// Get the requested page. By default grid sets this to 1.
	private Integer page = 0;

	// sorting order - asc or desc
	private String sord;

	// get index row - i.e. user click to sort.
	private String sidx;

	// Search Field
	private String searchField;

	// The Search String 
	private String searchString;

	// he Search Operation
	// ['eq','ne','lt','le','gt','ge','bw','bn','in','ni','ew','en','cn','nc']
	private String searchOper;

	// Your Total Pages
	private Integer total = 0;

	// All Record
	private Integer records = 0;

	/**
	 * This method is called for the action. Should handle data load, sorting,
	 * filtering etc.
	 * 
	 * The name is specified by programmer, see struts.xml for config.
	 * 
	 * @return
	 */
	public String listResults()  {
		selectedPeople = DataStorage.getDataStorage().getSelected();
		
		int to = (rows * page);
		// Count Rows Total
		if(selectedPeople != null) {
			records = selectedPeople.size();
		}
		to = Math.min(to, records); // we dont want the sublist to go wild, do
									// we

		int from = to - rows;

		// Your logic to search and select the required data.
		loadData(from, to);

		// calculate the total pages for the query
		total = (int) Math.ceil((double) records / (double) rows);
		
		return Action.SUCCESS;
	}
	
	/**
	 * Load data objects which have selected ids from the storage.
	 */
	private void loadData(int from, int to) {
		List<Person> fullData = new ArrayList<Person>();
		Person tmp;
		if(selectedPeople != null) {
		for (int id : selectedPeople) {
			tmp = dataStorage.getPersonById(id);
			if(tmp != null) {
				fullData.add(tmp);
			}
		}
		}
		
		int minIndex = Math.max(from, 0);
		minIndex = Math.min(minIndex, fullData.size());
		
		int maxIndex = Math.min(to, fullData.size());
		maxIndex = Math.max(maxIndex, 0);
		
		gridModel = fullData.subList(minIndex, maxIndex);
	}

	public List<Person> getGridModel() {
		return gridModel;
	}

	public void setGridModel(List<Person> gridModel) {
		this.gridModel = gridModel;
	}
	
	public Integer getRecords() {
		return records;
	}
	
	public void setRecords(Integer records) {
		this.records = records;
	}

	public Integer getRows() {
		return rows;
	}

	public void setRows(Integer rows) {
		this.rows = rows;
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public String getSord() {
		return sord;
	}

	public void setSord(String sord) {
		this.sord = sord;
	}

	public String getSidx() {
		return sidx;
	}

	public void setSidx(String sidx) {
		this.sidx = sidx;
	}

	public String getSearchField() {
		return searchField;
	}

	public void setSearchField(String searchField) {
		this.searchField = searchField;
	}

	public String getSearchString() {
		return searchString;
	}

	public void setSearchString(String searchString) {
		this.searchString = searchString;
	}

	public String getSearchOper() {
		return searchOper;
	}

	public void setSearchOper(String searchOper) {
		this.searchOper = searchOper;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}
	
}

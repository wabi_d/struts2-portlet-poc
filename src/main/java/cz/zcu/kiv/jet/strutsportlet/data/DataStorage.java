package cz.zcu.kiv.jet.strutsportlet.data;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class DataStorage {
	
	private static DataStorage _storage = null;

	private static int nextPersonId = 0;
	private List<Person> options = new ArrayList<Person>();
	private List<Integer> selected = new ArrayList<Integer>();
	
	
	protected DataStorage() {
		initOptionsPeople();
		
	}
	
	/**
	 * Init array with dummy date.
	 */
	@SuppressWarnings(value = { "deprecation" })//deprecated date constructor doesnt matter here really
	protected void initOptionsPeople() {
		this.addPerson(new Person("John", "Doe", "john.doe@portlet.te", new Date(84, 10, 22), Gender.MALE, false));
		this.addPerson(new Person("Jack", "Rabbit", "jazz@portlet.te", new Date(66, 9, 10), Gender.MALE, true));
		this.addPerson(new Person("Mark", "Knopfler", "dire.straits@portlet.te", new Date(81, 11, 2), Gender.MALE, true));
		this.addPerson(new Person("Eva", "Duarte", "evita@portlet.te", new Date(18, 4, 12), Gender.FEMALE, false));
		this.addPerson(new Person("Olivia", "Dunham", "thepattern@portlet.te", new Date(93, 5, 16), Gender.FEMALE, true));
	}
	
	
	public static DataStorage getDataStorage() {
		if(_storage == null) {
			_storage = new DataStorage();
		}
		return _storage;
	}
	
	public Person getPersonById(int id) {
		for (Person p : options) {
			if(p.getId() == id) {
				return p;
			}
		}
		
		return null;
	}
	
	public List<Person> getPeople() {
		return options;
	}
	
	public void addPerson(Person p) {
		p.setId(nextPersonId);
		nextPersonId++;
		options.add(p);
	}
	
	public List<Integer> getSelected() {
		return selected;
	}
	
	public void setSelected(List<Integer> selected) {
		this.selected = new ArrayList<Integer>(selected);
	}
}

package cz.zcu.kiv.jet.strutsportlet.actions;

public class UpdateNameAction extends DefaultPortletAction {

	private static final long serialVersionUID = 1L;

	private String firstName;

	private String lastName;

	@Override
	public String execute() throws Exception {
		getPortletPreferences().setValue("firstName", firstName);
		getPortletPreferences().setValue("lastName", lastName);
		getPortletPreferences().store();
		getActionMessages().add("Name updated");
		return SUCCESS;
	}
	
	@Override
	public String input() throws Exception {
		firstName = getPortletPreferences().getValue("firstName", "");
		lastName = getPortletPreferences().getValue("lastName", "");
		return INPUT;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getFirstName() {
		return firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getLastName() {
		return lastName;
	}

}

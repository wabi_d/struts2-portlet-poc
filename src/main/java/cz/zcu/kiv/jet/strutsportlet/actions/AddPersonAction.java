package cz.zcu.kiv.jet.strutsportlet.actions;

import cz.zcu.kiv.jet.strutsportlet.data.DataStorage;
import cz.zcu.kiv.jet.strutsportlet.data.Person;

/**
 * Controller for the first page's form.
 * @author veveri
 *
 */
public class AddPersonAction extends HelloAction {
	
	/**
	 * Newly added person.
	 */
	private Person personBean;

	public Person getPersonBean() {
		return personBean;
	}
	
	public void setPersonBean(Person personbean) {
		this.personBean = personbean;
	}

	@Override
	public String execute() throws Exception {
		DataStorage.getDataStorage().addPerson(personBean);
		personBean = null;
		return super.execute();
	}

}

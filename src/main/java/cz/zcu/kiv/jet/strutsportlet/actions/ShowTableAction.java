package cz.zcu.kiv.jet.strutsportlet.actions;

import java.util.List;
import cz.zcu.kiv.jet.strutsportlet.data.DataStorage;

/**
 * Action which takes list of submitted values (list selectedPeople) from previous page and
 * stores it inside "session" object. Doesn't directly provide data for the table.
 * 
 * That must be done by separate action due to json and portlet plugin incompatibility.
 * Table is controlled by LoadTableData class.
 * 
 * @author J. Danek
 */
public class ShowTableAction extends DefaultPortletAction {
	
	/**
	 * Data that come from the form.
	 */
	private List<Integer> selectedPeople;
	
	int count = 0;
	
	@Override
	public String execute() throws Exception {
		DataStorage.getDataStorage().setSelected(selectedPeople);
		
		count = selectedPeople.size();
		
		return SUCCESS;
	}
	
	public List<Integer> getSelectedPeople() {
		return selectedPeople;
	}
	
	public void setSelectedPeople(List<Integer> selectedPeople) {
		this.selectedPeople = selectedPeople;
	}

	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}

}

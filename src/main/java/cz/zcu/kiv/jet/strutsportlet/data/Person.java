package cz.zcu.kiv.jet.strutsportlet.data;

import java.util.Date;

public class Person {
	
		private int id;
		private String firstName;
	    private String lastName;
	    private String email;
	    private Date birthDay;
	    private Gender gender;
	    private boolean superHero;

	    public String getFirstName()
	    {
	        return firstName;
	    }

	    public void setFirstName(String firstName)
	    {
	        this.firstName = firstName;
	    }

	    public String getLastName()
	    {
	        return lastName;
	    }

	    public void setLastName(String lastName)
	    {
	        this.lastName = lastName;
	    }

	    public String getEmail()
	    {
	        return email;
	    }

	    public void setEmail(String email)
	    {
	        this.email = email;
	    }
	    
	    public void setBirthDay(Date birthDay) {
			this.birthDay = birthDay;
		}
	    
	    public Date getBirthDay() {
			return birthDay;
		}
	    
	    public String getFullName() {
	    	return this.firstName + " " + this.lastName;
	    }
	    
	    public int getId() {
			return id;
		}
	    public void setId(int id) {
			this.id = id;
		}
	    
	    public Gender getGender() {
			return gender;
		}
	    
	    public void setGender(Gender gender) {
			this.gender = gender;
		}

	    public boolean isSuperHero() {
			return superHero;
		}
	    
	    public void setSuperHero(boolean superHero) {
			this.superHero = superHero;
		}
	    
	    public Person(String firstName, String lastName, String email, Date bday, Gender gender, boolean superHero) {    	
	    	this.firstName = firstName;
	    	this.lastName = lastName;
	    	this.email = email;
	    	this.birthDay = bday;
	    	this.gender = gender;
	    	this.superHero = superHero;
		}
	    
	    public Person() {
			// TODO Auto-generated constructor stub
		}

}

<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>

<link rel="stylesheet" type="text/css" href='<s:url value="/struts/themes/smoothness/jquery-ui.css?s2j=3.5.1"/>'></link>
<script type="text/javascript" src='<s:url value="/struts/js/base/jquery-1.8.3.min.js"></s:url>'></script>
<script type="text/javascript" src='<s:url value="/struts/js/base/jquery.ui.core.min.js?s2j=3.5.1"/>'></script>
<script type="text/javascript" src='<s:url value="/struts/js/plugins/jquery.subscribe.min.js"/>'></script>
<script type="text/javascript" src='<s:url value="/struts/js/struts2/jquery.struts2.min.js?s2j=3.5.1"/>'></script>
<script type="text/javascript" src='<s:url value="/struts/js/struts2/jquery.ui.struts2.min.js"/>'></script>

<script type="text/javascript">
$(function() {
	jQuery.struts2_jquery.version="3.5.1";
  	jQuery.scriptPath = "${pageContext.request.contextPath}/struts/";
	jQuery.ajaxSettings.traditional = true;

	jQuery.ajaxSetup ({
		cache: false
	});
	
	jQuery.struts2_jquery.require("js/struts2/jquery.ui.struts2.min.js");
	
});

function formatBool(cellvalue, options, rowObject) {
	if(cellvalue == true) {
		return "Yes";
	} else {
		return "No";
	}
}

</script>

Total line count: <s:property value="count"/>

<!-- Url for json returning controller. Not .action suffix which results in 
different servlet handling the call (see web.xml). JSON and PortletServlet dont
work well together.
 -->
<s:url id="remoteurl" value="/view/ajax/fillTable.action"/>
<sjg:grid
         id="gridtable"
        caption="Person Examples"
        dataType="json"
        href="%{remoteurl}"
        pager="true"
        gridModel="gridModel"
        rowList="10,15,20"
        rowNum="15"
        rownumbers="true"
    >
        <sjg:gridColumn name="id" index="id" title="ID" formatter="integer" sortable="false"/>
        <sjg:gridColumn name="firstName" index="name" title="First Name" sortable="true"/>
        <sjg:gridColumn name="lastName" index="country" title="Last Name" sortable="false"/>
        <sjg:gridColumn name="email" index="city" title="Email" sortable="false"/>
        <sjg:gridColumn name="gender" index="gender" title="Gender" sortable="false"/>
        <sjg:gridColumn name="birthDay" index="creditLimit" title="Bday" formatter="date" sortable="false"/>
        <sjg:gridColumn name="superHero" index="superHero" title="Super Hero" formatter="formatBool" sortable="false"/>
    </sjg:grid>
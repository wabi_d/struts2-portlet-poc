<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<link rel="stylesheet" type="text/css" href='<s:url value="/struts/themes/smoothness/jquery-ui.css?s2j=3.5.1"/>'></link>
<script type="text/javascript" src='<s:url value="/struts/js/base/jquery-1.8.3.min.js"></s:url>'></script>
<script type="text/javascript" src='<s:url value="/struts/js/base/jquery.ui.core.min.js?s2j=3.5.1"/>'></script>
<script type="text/javascript" src='<s:url value="/struts/js/plugins/jquery.subscribe.min.js"/>'></script>
<script type="text/javascript" src='<s:url value="/struts/js/struts2/jquery.struts2.min.js?s2j=3.5.1"/>'></script>
<script type="text/javascript" src='<s:url value="/struts/js/struts2/jquery.ui.struts2.min.js"/>'></script>

<script type="text/javascript">
$(function() {
	jQuery.struts2_jquery.version="3.5.1";
  	jQuery.scriptPath = "${pageContext.request.contextPath}/struts/";
	jQuery.ajaxSettings.traditional = true;

	jQuery.ajaxSetup ({
		cache: false
	});
	
	jQuery.struts2_jquery.require("/js/struts2/jquery.ui.struts2.min.js");
	
});
</script>




<!-- Actual Page Code -->
<!--  portlet preferences bound information -->
<s:if test='%{firstName != "" || lastName != ""}'>
	<h2>Hello, <s:property value="firstName"/> <s:property value="lastName"/></h2>
</s:if>
<s:else>
    <b>No name set. Go to <a href="<s:url action="index" method="input" portletMode="edit"/>">edit mode</a> to set your name</b>
</s:else>
<br/>
<br/>

<!-- ListBox with people in data storage. Form submit redirects to the page with a table
see struts.xml for mapping.
-->
<h3>List of People:</h3>
Select people and hit the Bulb button to display details.
<s:form portletMode="view" action="exportToTable">

<s:select list="people" 
		multiple="true"
 		name="selectedPeople"
  		listKey="id"
  		size="10"
		listValue="fullName"/>
		
		<!-- TODO resolve to context relative path... -->
		<s:submit value="Export To Table" type="image" src="/strutsportlet/images/dialog-information.png"/>  
</s:form>

<sj:a openDialog="mydialogModal">Add person - modal</sj:a>
<sj:a openDialog="mydialog">Add person - no modal</sj:a>

<sj:dialog id="mydialogModal" href="#" 
title="Dialog open on Click" modal="true" 
closeTopics="closeThisDialog" autoOpen="false">

<h3>Add new person:</h3>
<s:form action="addPerson">

 	  <s:textfield name="personBean.firstName" label="First name" />
 	  <s:textfield  name="personBean.lastName" label="Last name" />
 	  <s:textfield name="personBean.email"  label ="Email"/>  
 	  <sj:datepicker  id="date0" name="personBean.birthDay" label="Date of Birth" showButtonPanel="true"/>
 	  <s:radio label="Gender" name="personBean.gender" list="genders"/>
 	  <s:checkbox name="personBean.superHero" label="Are you a superhero?"/>
 	  
 	  <sj:submit onCompleteTopics="closeThisDialog"/>
</s:form>
</sj:dialog>

<sj:dialog id="mydialog" href="#" 
title="Dialog open on Click" modal="false" 
closeTopics="closeThisDialog" autoOpen="false">

<h3>Add new person:</h3>
<s:form action="addPerson">

 	  <s:textfield name="personBean.firstName" label="First name" />
 	  <s:textfield  name="personBean.lastName" label="Last name" />
 	  <s:textfield name="personBean.email"  label ="Email"/>  
 	  <sj:datepicker  id="date1" name="personBean.birthDay" label="Date of Birth" showButtonPanel="true"/>
 	  <s:radio label="Gender" name="personBean.gender" list="genders"/>
 	  <s:checkbox name="personBean.superHero" label="Are you a superhero?"/>
 	  
 	  <sj:submit onCompleteTopics="closeThisDialog"/>
</s:form>
</sj:dialog>
